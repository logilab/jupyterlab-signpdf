import React, { useState, useEffect } from "react";
import { ReactWidget } from "@jupyterlab/apputils";
import { ServerConnection } from "@jupyterlab/services";

import { request } from "./utils";

/**
 * React component for a counter.
 *
 * @returns The React component
 */
const IframeComponent = (): JSX.Element => {
  const [iframeUrl, setIframeUrl] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      await request(
        "handleUrl",
        "GET",
        {},
        ServerConnection.makeSettings(),
      ).then((data) => {
        setIframeUrl(data);
      });
    };

    fetchData().catch(console.error);
  }, []);

  return (
    <div>
      <iframe className="jp-Signpdf-Widget" src={iframeUrl} />
    </div>
  );
};

/**
 * A Counter Lumino Widget that wraps a IframeComponent.
 */
export class IframeWidget extends ReactWidget {
  /**
   * Constructs a new IframeWidget.
   */

  render(): JSX.Element {
    return <IframeComponent />;
  }
}
