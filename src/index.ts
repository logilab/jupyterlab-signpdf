import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin,
} from "@jupyterlab/application";
import { MainAreaWidget, ICommandPalette } from "@jupyterlab/apputils";
import { ILauncher } from "@jupyterlab/launcher";
import { LabIcon } from "@jupyterlab/ui-components";

import { IframeWidget } from "./widget";
import { PdfWidget } from "./pdfWidget";

import pdfIconStr from "../style/PDF_icon.svg";

/**
 * The command IDs used by the react-widget plugin.
 */
namespace CommandIDs {
  export const create = "sign-pdf";
  export const url = "url-pdf";
}

/**
 * Initialization data for the react-widget extension.
 */
const extension: JupyterFrontEndPlugin<void> = {
  id: "signpdf",
  autoStart: true,
  requires: [ICommandPalette],
  optional: [ILauncher],
  activate: (
    app: JupyterFrontEnd,
    palette: ICommandPalette,
    launcher: ILauncher,
  ) => {
    const { commands } = app;
    const icon = new LabIcon({
      name: "launcher:signPDF",
      svgstr: pdfIconStr,
    });
    let command = CommandIDs.create;
    commands.addCommand(command, {
      caption: "Open signpdf",
      label: "Sign PDF",
      icon: (args) => (args["isPalette"] ? null : icon),
      execute: () => {
        const content = new IframeWidget();
        const widget = new MainAreaWidget<IframeWidget>({ content });
        widget.title.label = "SignPDF";
        widget.title.icon = icon;
        app.shell.add(widget, "main");
      },
    });
    if (launcher) {
      launcher.add({
        command,
        category: "Apps",
      });
    }
    // trainer button
    command = CommandIDs.url;
    commands.addCommand(command, {
      label: "pdf file url",
      caption: "https://pdf.logilab.fr/signature/[XXXX]",
      execute: () => {
        const content = new PdfWidget();
        const widget = new MainAreaWidget<PdfWidget>({ content });
        widget.title.label = "PDF url";
        app.shell.add(widget, "main");
      },
    });
    console.log("JupyterLab extension jupyterlab-sign-pdf is activated!");
    palette.addItem({ command, category: "Training" }); // menu is in jupyterlabtraining extension
  },
};

export default extension;
