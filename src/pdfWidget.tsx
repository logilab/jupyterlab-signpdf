import React, { useState } from "react";
import { ReactWidget } from "@jupyterlab/apputils";
import { ServerConnection } from "@jupyterlab/services";

import { request } from "./utils";

export const PdfComponent = (): JSX.Element => {
  const [url, setUrl] = useState("");
  const [message, setMessage] = useState("");

  const saveUrl = (e: React.ChangeEvent<HTMLInputElement>) => {
    setUrl(e.currentTarget.value);
  };
  const sendPdfUrl = () => {
    request(
      "handleUrl",
      "POST",
      JSON.stringify({
        url: url,
      }),
      ServerConnection.makeSettings(),
    );
    setMessage("done");
  };

  return (
    <div style={{ margin: "4em" }}>
      <h1>SignPdf url</h1>
      <input value={url} size={50} onChange={saveUrl} />
      <br />
      <br />
      <button
        className="btn btn-sm btn-primary"
        onClick={(): void => {
          sendPdfUrl();
        }}
      >
        Send
      </button>
      <br />
      {message}
    </div>
  );
};

export class PdfWidget extends ReactWidget {
  render(): JSX.Element {
    return <PdfComponent />;
  }
}
