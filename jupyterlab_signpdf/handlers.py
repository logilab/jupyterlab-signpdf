import json
import os

import tornado
from jupyter_server.utils import url_path_join
from jupyter_server.base.handlers import APIHandler

path = os.path.expanduser("~/files/project_shared")
if not os.path.exists(path):
    path = os.path.expanduser("/tmp")
json_path = os.path.join(path, "signpdf.json")


def setup_handlers(web_app):
    host_pattern = ".*$"
    web_app.add_handlers(
        host_pattern,
        [
            # write sgnpdf url
            (
                url_path_join(
                    web_app.settings["base_url"], "/jupyterlab-signpdf/handleUrl"
                ),
                SignPDFAPIHandler,
            ),
        ]
    )


class SignPDFAPIHandler(APIHandler):

    @tornado.web.authenticated
    def post(self) -> None:
        data = json.loads(self.request.body.decode("utf-8"))
        if "url" in data:
            if "pdf.logilab.fr" not in data['url']:
                os.remove(json_path)
            else:
                with open(json_path, "w") as f:
                    f.write(data["url"])
        self.finish(json.dumps())

    @tornado.web.authenticated
    def get(self) -> None:
        url = "https://pdf.logilab.fr"
        if os.path.exists(json_path):
            with open(json_path) as f:
                url = f.read()
        self.finish(json.dumps(url))
