from .handlers import setup_handlers
from ._version import __version__


def _jupyter_labextension_paths():
    return [{
        'src': 'labextension',
        'dest': "jupyterlab_signpdf",
    }]


def _jupyter_server_extension_points():
    return [{
        "module": "jupyterlab_signpdf"
    }]


def _load_jupyter_server_extension(server_app):
    """Registers the API handler to receive HTTP
    requests from the frontend extension.

    Parameters
    ----------
    lab_app: jupyterlab.labapp.LabApp
        JupyterLab application instance
    """
    setup_handlers(server_app.web_app)
    server_app.log.info(
        "Registered Jupyterlab_signpdf server extension"
    )


# For backward compatibility with notebook server - useful for Binder/JupyterHub
load_jupyter_server_extension = _load_jupyter_server_extension
